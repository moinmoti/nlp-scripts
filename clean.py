#!/usr/bin/python3

import os

path1 = "./original"
path2 = "./cleaned"

if not os.path.exists(path2):
    os.mkdir(path2)

for filename in os.listdir(path1):
    # copy_flag = False
    with open(os.path.join(path1, filename)) as f1:
        lines = []

        for line in f1:
            if "All rights reserved".lower() in line.lower() and "Factiva" in line:
                parts = line.split(".")
                new_parts = []
                ignore_flag = False

                for part in parts:
                    if ignore_flag is True:
                        ignore_flag = False

                        continue
                    elif "\\'a9 2019 Factiva, Inc" in part:
                        part = part.replace(" \\'a9 2019 Factiva, Inc", "")
                        new_parts.append(part)
                        ignore_flag = True
                    else:
                        new_parts.append("\n")
                line = ".".join(new_parts)

            if "***" in line and "Page" in line:
                parts = line.split()
                new_parts = []

                for part in parts:
                    if ("***" not in part) and ("Page" not in part):
                        new_parts.append(part)
                line = " ".join(new_parts)
            lines.append(line)

    with open(os.path.join(path2, filename), 'w') as f2:
        f2.writelines(lines)
