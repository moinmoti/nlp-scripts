#!/usr/bin/python3
import os

from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

path1 = "./lowercased"
path2 = "./stopped"

if not os.path.exists(path2):
    os.mkdir(path2)

stop_words = set(stopwords.words('english'))

for filename in os.listdir(path1):
    lines = []
    with open(os.path.join(filename, path1)) as f1:
        for line in f1:
            tokens = word_tokenize(line)
            line = ' '.join([token for token in tokens if token not in stop_words])
            lines.append(line)

    with open(os.path.join(filename, path2), 'w') as f2:
        f2.writelines(lines)
