#!/usr/bin/python3

import os

path1 = "./cleaned"
path2 = "./lowercased"

if not os.path.exists(path2):
    os.mkdir(path2)

for filename in os.listdir(path1):
    lines = []
    with open(os.path.join(path1, filename)) as f1:
        for line in f1:
            lines.append(line.lower())

    with open(os.path.join(path2, filename), 'w') as f2:
        f2.writelines(lines)
