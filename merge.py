#!/usr/bin/python3

import os

path1 = "./lemmatized"
path2 = "./merged"

lines = []

if not os.path.exists(path2):
    os.mkdir(path2)

for filename in os.listdir(path1):
    with open(os.path.join(path1, filename)) as f1:
        for line in f1:
            lines.append(line)

fn2 = os.path.join(path2, "merged.txt")
with open(fn2, 'w') as f2:
    f2.writelines(lines)
