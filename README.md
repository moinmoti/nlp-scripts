# NLP-Scripts

## Instructions to clone this repo
`git clone https://gitlab.com/moinmoti/nlp-scripts.git`

> Note: This is a private repo, hence unless you are a project member, you will need the Maintainer's (moinmoti@outlook.com) permission to access the repo.

This repository consists of six scripts, namely
- clean.py
- lemmatize.py
- lowercase.py
- merge.py
- stem.py
- stop.py

## Pre-requisite
- Create an empty directory named *original*.

`mkdir original`

- Please keep all your original files in the *original* director created above.
> Do not worry, any file under original directory or any other directory created through the script won't be tracked (see *.gitignore*)

## Steps for processing your code
Please adhere to the following sequence of steps as the scripts use the output of the files generated in previous steps.

### 1 Cleaning
Usage instructions:

`python3 clean.py`

- The *clean.py* script cleans the unnecessary clutter in the files (Footnotes in this case). The script can be changed accordingly depending on user's specfications.
- This script will take all files present in the *original* directory and generate the corresponding cleaned file of the same filename in *cleaned* directory (automatically created by the script).

### 2 Lowercasing
Usage instructions:

`python3 lowercase.py`

- *lowercase.py* converts the entire content of to lowercase characters.
- This script takes each file in *cleaned* directory (created in step 1), and generates the corresponding lowercased file with same filename in *lowercased* directory (automatically created by the script).

### 3 Removing Stopwords
Usage instructions:

Install *nltk* library:

`python3 -m pip install --user --upgrade nltk`

Then inside python3 interactive shell, perform the following commands:
```python
import nltk
nltk.download("stopwords")
nltk.download("punkt")
```
Finally, exit the python3 shell and execute the script:

`python3 stop.py`

- *stop.py* removes all the stopwords from the files. It uses the *nltk* library.
- This script takes each file in *lowercased* directory (created in step 2), and generates the corresponding stopped (stopwords removed) file with same filename in *stopped* directory (automatically created by the script).

### 4.1 Lemmatizing
Usage instructions:

`python3 lemmatize.py`

- *lemmatize.py* lemmatizes all the files.
- This script takes each file in *stopped* directory (created in step 3), and generates the corresponding lemmatized file with same filename in *lemmatized* directory (automatically created by the script).

### 4.2 Stemming
Usage instructions:

`python3 stem.py`

- *stem.py* stems all the files.
- This script takes each file in *stopped* directory (created in step 3), and generates the corresponding stemmed file with same filename in *stemmed* directory (automatically created by the script).

### 5 Merging
Usage instructions:

`python3 merge.py`

- *merge.py* merges all the files present in the *lemmatized* directory.
- This script takes each file in *lemmatized* directory (created in step 4.1), and generates a new file named *merged.py* in *merged* directory (automatically created by the script).
- Please note that, in order to merge all files in *stopped* directory or any other directory instead of *lemmatized* directory, change the following line of code:
```python
path1 = "./stopped"
```
