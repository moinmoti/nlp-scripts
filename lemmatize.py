#!/usr/bin/python3

import os

from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize

path1 = "./stopped"
path2 = "./lemmatized"

if not os.path.exists(path2):
    os.mkdir(path2)

lemmatizer = WordNetLemmatizer()

for filename in os.listdir(path1):
    lines = []

    with open(os.path.join(path1, filename)) as f1:
        for line in f1:
            tokens = word_tokenize(line)
            line = ' '.join([lemmatizer.lemmatize(token) for token in tokens])
            lines.append(line)

    with open(os.path.join(path2, filename), 'w') as f2:
        f2.writelines(lines)
